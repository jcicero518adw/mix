
const doSomething = async(name) => {
    return await new Promise((resolve, reject) => {
        resolve(`Yo ${name}!`);
    })
};

doSomething('Man').then(response => {
    document.write(response);
});
