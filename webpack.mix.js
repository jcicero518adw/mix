const mix = require('laravel-mix');

/**
 * Vanilla
 */
mix.sass('src/scss/main.scss', 'dist').js('src/js/main.js', 'dist');

/**
 * React
 */
// mix.sass('src/scss/main.scss', 'dist').react('src/js/main.js', 'dist');
